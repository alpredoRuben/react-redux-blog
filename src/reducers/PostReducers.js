
const removeItemsById = (items, cases) => {
    return items.id !== cases.id;
}

const PostReducers = (state=[], action) => {

    console.log(state);

    /** type adalah variable bawaan */
    switch (action.type) {
        case 'ADD_POST':
            return state.concat([action.penampung]);
        case 'DELETE_POST':
            return state.filter((post) => removeItemsById(post, action));
        case 'EDIT_POST':
            return state.map( (post) => post.id === action.id ? {...post, editing: !post.editing} : post)
        case 'UPDATE_DATA':
            return state.map(
                (post) => {
                    if(post.id === action.id) {
                        return {
                            ...post,
                            title: action.penampung.editTitle,
                            message: action.penampung.editMessage,
                            editing: !post.editing
                        }
                    }
                    else{
                        return post;
                    }
                }
            );

        default:
            return state;
    }
}

export default PostReducers ;
