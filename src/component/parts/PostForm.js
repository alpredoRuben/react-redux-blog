import React, { Component } from 'react';
import { connect } from 'react-redux';

export default connect()(
    class PostForm extends Component {

        eventButtonSubmitHandle = (e) => {
            e.preventDefault();

            const title = this.getTitle.value;
            const message = this.getBody.value;
            const data = {
                id: new Date(),
                title,
                message,
                editing:false
            };

            //console.log(data);

            this.props.dispatch({ type: 'ADD_POST', penampung: data });

            this.getTitle.value = '';
            this.getBody.value = '';
        }

        render() {
            return(
                <div className="post-container">
                    <h1 className="post-heading">Tambah Content</h1>
                    <form className="form" onSubmit={this.eventButtonSubmitHandle}>
                        <input required type="text" placeholder="Enter Post Title" ref={(input)=>this.getTitle = input}  /><br /><br />
                        <textarea required rows="5" cols="28" placeholder="Enter Post" ref={(input)=>this.getBody = input} /><br /><br />
                        <button>Post</button>
                    </form>
                </div>
            );
        }

    }
)