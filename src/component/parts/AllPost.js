import React from 'react';
import { connect } from 'react-redux';
import PostContainer from '../containers/PostContainer';
import EditContainer from '../containers/EditContainer';

const mapStateToProps = (state) => {
    return {
        posts: state
    }
}

export default connect(mapStateToProps) (
    class AllPost extends React.Component {

        render() {
            
            return (
                <div>
                    <h2 className="post_heading">All Posts</h2>
                    {
                        this.props.posts.map((results) => (
                            <div key={results.id}>
                                {
                                    (results.editing) ? <EditContainer attrData={results} key={results.id} />:<PostContainer attrData={results} key={results.id} />
                                }   
                            </div>
                        ))
                    }
                </div>
            );
        }

    }
);

