import React from 'react';
import {connect} from 'react-redux';

export default connect()(
    class PostContainer extends React.Component {

        eventDeleteHandler = (params) => {
            params.dispatch({
                type: 'DELETE_POST',
                id: params.attrData.id
            });
        }

        eventEditHandler = (params) => {
            params.dispatch({
                type: 'EDIT_POST',
                id: params.attrData.id
            });

            console.log(params.attrData.editing);
        }

    
        render() {
            return (
                <div className="post">
                    <h4 className="post_title">{this.props.attrData.title}</h4>
                    <p className="post_message"><i>{this.props.attrData.message}</i></p>
                    <div className="control-buttons">
                        <button className="edit" onClick={() => this.eventEditHandler(this.props)}>
                            Edit
                        </button>
                        <button className="delete" onClick={()=> this.eventDeleteHandler(this.props)}>
                            Delete
                        </button>
                    </div>
                </div>
            );
        }
    }
);