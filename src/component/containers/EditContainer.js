import React from 'react';
import { connect } from 'react-redux';

export default connect()(
    class EditContainer extends React.Component {
        editEventHandler = (e) => {
            const newEditData = {
                editTitle: this.getTitle.value,
                editMessage: this.getMessage.value
            };

            this.props.dispatch({ 
                type: 'UPDATE_DATA', 
                id: this.props.attrData.id, 
                penampung: newEditData 
            });
        }

        render() {
            return (
                <div className="post">
                    <form className="form" onSubmit={this.editEventHandler}>
                        <input required type="text" ref={(input) => this.getTitle = input} defaultValue={this.props.attrData.title} placeholder="Enter Post Title" /><br /><br />
                        <textarea required rows="5" ref={(input) => this.getMessage = input} defaultValue={this.props.attrData.message} cols="28" placeholder="Enter Post" /><br /><br />
                        <button>Update</button>
                    </form>
                </div>
            );
        }
    }
);
