import React, { Component } from 'react';
import './App.css';
import PostForm from './component/parts/PostForm';
import AllPost from './component/parts/AllPost';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="navbar">
          <h2 className="center ">Implementasi React Redux</h2>
        </div>
        <PostForm />
        <AllPost />
      </div>
    );
  }
}

export default App;
